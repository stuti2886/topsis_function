source("F:/research_BKS/Topsis_functions.R")

data = read.csv("F:/research_BKS/agricultural_Data.csv", header = T, stringsAsFactors = F)

row.names(data) = data[,1]

library(Hmisc)
var_clust = varclus(data, similarity = "spearman", type = "data.matrix")

head(data)
data_Soil = subset(data, Sample == "Soil", select = -c(Sample))
data_SM = subset(data, Sample == "SM", select = -c(Sample))
data_SMB = subset(data, Sample == "SMB", select = -c(Sample))

data_cont = as.matrix(subset(data, select= -c(Method,Sample)))
dim(data_cont)
var_clust = varclus(data_cont, similarity = "spearman", type = "data.matrix")
plot(var_clust)
list(var_clust)

install.packages("ClustOfVar")
library("ClustOfVar")

tree <- hclustvar(data_cont)
t1 = kmeansvar(X.quanti = data_cont, X.quali = NULL, 3, iter.max = 150,
          nstart = 1, matsim = FALSE)
list(t1)
t1$cluster

plot(tree)
stab <- stability(tree,B=40)
plot(stab, main="Stability of the partitions")
stab$matCR
boxplot(stab$matCR, main="Dispersion of the ajusted Rand index")
P3<-cutreevar(tree,3)
cluster <- P3$cluster

clust1 = hclust(t(data_cont))
?hclust
data_Soil1 = data_Soil[,1]
data_SM1 = data_SM[,1]
data_SMB1= data_SMB[,1]

data_Soil2 = data_Soil[,-1]
data_SM2 = data_SM[,-1]
data_SMB2= data_SMB[,-1]
#paper based on c1, c3, c6
CI_Soil = cbind(data_Soil1,wt_CI(data_Soil2))
CI_SM = cbind(data_SM1,wt_CI(data_SM2))
CI_SMB = cbind(data_SMB1,wt_CI(data_SMB2))

data_temp = standardize(data)

scale(data[,3], center = T, scale = T)

data_1 = (data[,3]- mean(data[,3]))/sd(data[,3])

data_1 = (data[,3]- min(data[,3]))/(max(data[,3]) - min(data[,3]))
max(data_1)
min(data_1)

ks.test(data_1,"punif",0,1)

